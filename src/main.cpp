#include "raylib.h"
#include <iostream>
#include "ScreenSize.h"
#include "Scenes/MainMenu/MainMenu.h"

void main()
{
    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
    SetTargetFPS(60);  
    mainmenu::MainMenu* mainmenu = new mainmenu::MainMenu();
    mainmenu->play(); 
}