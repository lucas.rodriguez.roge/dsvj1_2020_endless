#pragma once
#ifndef GAME_H
#define GAME_H
#include "raylib.h"
#include "../../ScreenSize.h"
#include "../../Entities/Player/Player.h"
#include "../../Entities/Obstacles/Obstacles.h"
#include "../../Entities/Powerups/Powerups.h"
#include "../../Entities/Spells/Spells.h"
#include "../../Entities/Sound/Sound.h"
#include "../../Entities/Options/Options.h"

namespace gameplay {
	
	const short posground = screenHeight - 119;
	const short poshud = screenHeight - 40;
	const short minmana = 0;
	const short cantgroundmovements = 4;
	const short cantbackgroundmovements = 3;
	const short cantbackgroundsprites = 4;
	const short canttreemovements = 3;
	const short movementground = 5;
	const short movementbat = 9;
	const short movementfireball = 5;
	const short durationshield = 5;
	const short posup = 50;
	const short posmiddle = 150;
	const short cantopciones = 2;

	class Game {
	private:
		spells::Fireball* fireball;
		float _fireballmovement;
		float _proceduralmovement;
		obstacle::Bat* bat;
		float _batmovement;
		float _posbat;
		obstacle::Tree* tree;
		short _treetype;
		float _treemovement;
		Player* player;
		options::Options* options[cantopciones];
		CHOISE _input;
		powerups::Shield* shield;
		powerups::Potion* potion;
		short _potionmovement;
		bool _gameover;
		bool _pause;
		Texture2D _ground;
		float _groundmovement[cantgroundmovements];
		Texture2D _background[cantbackgroundsprites];
		float _backgroundmovement[cantbackgroundsprites][cantbackgroundmovements];
		Texture2D _heart;
		Texture2D _manabar;
		short _cantlifes;
		int _time;
		int _seconds;
		int _minutes;
		bool _spawn;
		bool _collisionflag;
		bool _shootflag;
		short _timeelapsedshield;
		short _mana;

	public:
		void init();
		void update(short& score);
		void draw(short& _score);
		void reinit();
		void play(short& score);
		void hud(short& score);
		void spawnObstacle();
		void collisionManager(short& score);
		void spawnPotion();
		void pauseMenu();
	};
}
#endif // !GAME_H

