#include "Game.h"
#include <iostream>

namespace gameplay {
	void Game::init() {
		PlayMusicStream(sound::musicgame);
		_proceduralmovement = 0;
		_input = CHOISE::RESUME;
		fireball = new spells::Fireball();
		fireball->init();
		_fireballmovement = 0;
		player = new Player();
		player->init();
		tree = new obstacle::Tree();
		tree->init();
		bat = new obstacle::Bat();
		_treetype = GetRandomValue(1, 2);
		bat->init();
		_posbat = GetRandomValue(1, 2);
		if (_posbat == 1) {
			_posbat = posup;
		}
		Vector2 posresume;
		Vector2 posgoback;

		posresume.x = screenWidth / 2 - 273 / 2; //_options[0]->getTextBox().width/2;
		posresume.y = screenHeight / 3;
		options[0] = new options::Options("RESUME", posresume);
		posgoback.x= screenWidth / 2 - options[0]->getTextBox().width / 2;
		posgoback.y= options[0]->getPos().y + options[0]->getTextBox().height;
		options[1] = new options::Options("GOBACK", posgoback);
		_posbat = posmiddle;
		_batmovement = screenWidth;
		_ground = LoadTexture("res/Textures/Background/ground.png");
		shield = new powerups::Shield();
		potion = new powerups::Potion();
		potion->init();
		potion->setBoxcolliderX(screenWidth);
		potion->setBoxcolliderY(posground - potion->getBoxcolliderheight());
		_treemovement = -tree->getWidth();
		short aux1 = -_ground.width;
		for (short i = 0;i < cantgroundmovements;i++) {
			_groundmovement[i] = aux1;
			aux1 += _ground.width;
		}
		for (short j = 0;j < cantbackgroundsprites;j++) {
			short aux2 = -1024;
			for (short i = 0;i < cantbackgroundmovements;i++) {
				_backgroundmovement[j][i] = aux2;
				aux2 += 1024;
			}
		}
		_background[0] = LoadTexture("res/Textures/Background/background1.png");
		_background[1] = LoadTexture("res/Textures/Background/background2.png");
		_background[2] = LoadTexture("res/Textures/Background/background3.png");
		_background[3] = LoadTexture("res/Textures/Background/background4.png");
		_heart = LoadTexture("res/Textures/Background/heart.png");
		_manabar = LoadTexture("res/Textures/Background/manabar.png");
		_gameover = false;
		_spawn = false;
		_collisionflag = false;
		_shootflag = false;
		_pause = false;
		_cantlifes = 6;
		_time = 0;
		_seconds = 0;
		_minutes = 0;
		_mana = 10;
	}
	void Game::update(short & _score) {
		if (!_pause) {
			UpdateMusicStream(sound::musicgame);
			player->update();
			if (_time < 60)
				_time++;
			else {
				_score += 10;
				_time = 0;
				if (_seconds < 59) { //1 SECOND
					_seconds++;
					if (_collisionflag == true) {
						_timeelapsedshield--;
					}
					if (_seconds % 10 == 0) {
						_proceduralmovement -= GetFrameTime() * 150;
					}
				}
				else {
					_seconds = 0; //1 MINUTE
					_minutes++;
					_score += 100;
				}
				if (_timeelapsedshield % 5 == 0) {
					_collisionflag = false; //ANTICOLLISION SHIELD DURATION
				}
				if (_seconds % 2 == 0) {
					_spawn = true;
					if (!bat->getActive()) {
						_batmovement = screenWidth;
						bat->setActive(true);
					}
					if (!tree->getActive()) {
						_treemovement = screenWidth;
						tree->setActive(true);
					}
				}
				if (_seconds % 11 == 0) {
					_potionmovement = screenWidth;
					potion->setActive(true);
				}
			}
			collisionManager(_score);
			if (IsKeyPressed(KEY_P)) {
				_pause = true;
			}
		}
	}
	void Game::draw(short& _score) {
		BeginDrawing();
		ClearBackground(BLACK);
		for (short j = cantbackgroundsprites - 1;j >= 0;j--) {
			for (short i = 0;i < cantbackgroundmovements;i++) {
				DrawTexture(_background[j], _backgroundmovement[j][i], 0, WHITE);
				if (_backgroundmovement[j][i] >= -_background[j].width) {
					switch (j) {
					case 3:
						if (!_pause)
						_backgroundmovement[j][i] += (_proceduralmovement / 2) - 1;
						break;
					case 2:
						if (!_pause)
						_backgroundmovement[j][i] += (_proceduralmovement / 2) - 3;
						break;							//PARALAX 4 BACKGROUND PICTURE MOVEMENT
					case 1:
						if (!_pause)
						_backgroundmovement[j][i] += (_proceduralmovement / 2) - 6;
						break;
					case 0:
						if (!_pause)
						_backgroundmovement[j][i] += (_proceduralmovement / 2) - 9;
						break;
					}
					if (!_pause)
					_backgroundmovement[j][i] += (_proceduralmovement / 2) - 1;
				}
				else _backgroundmovement[j][i] = screenWidth;
			}
		}
		for (short i = 0;i < cantgroundmovements;i++) {
			if (_groundmovement[i] >= -_ground.width) {
				if (!_pause)
					_groundmovement[i] += _proceduralmovement - movementground;
			}
			else {
				if (!_pause)
				_groundmovement[i] = _groundmovement[(i + 3) % 4] + _ground.width + (_proceduralmovement - movementground);
			}
			DrawTexture(_ground, _groundmovement[i], posground, WHITE);
		}
		if (_cantlifes < 0) {
			_gameover = true;
		}
		spawnObstacle();
		spawnPotion();
		player->draw();
		if (_collisionflag == true) {
			shield->setBoxcolliderX((player->getSizeposx() + player->getSizeposwidth()));
			shield->setBoxcolliderY((player->getSizeposy() - player->getSizeposheight() / 2));
			if (_timeelapsedshield > 2)
				shield->draw();
			else {
				if (_time > 30) {
					shield->draw();
				}
			}
		}
		if (IsKeyPressed(KEY_SPACE) && !_shootflag && _mana > 0 && !_pause) {
			PlaySound(sound::shoot);
			_shootflag = true;
			_mana--;
			fireball->setActive(true);
			_fireballmovement = player->getBoxcollider().x - 210;
			fireball->setBoxColliderY(player->getSizeposy() + player->getSizeposheight() / 2);
		}
		if (_shootflag) {
			if (fireball->getActive()) {
				fireball->setBoxColliderX(player->getSizeposx() + player->getSizeposwidth());
				if (_fireballmovement <= screenWidth && !_pause) {
					fireball->setBoxColliderX(fireball->getBoxColliderX() + _proceduralmovement + _fireballmovement);
					_fireballmovement += movementfireball;
				}
				else {
					_shootflag = false;
					_fireballmovement = -155;
					fireball->setBoxColliderX(-155);
				}
				fireball->draw();
			}
		}
		hud(_score);
		if (_pause)
		pauseMenu();
	EndDrawing();
	}
	void Game::reinit() {
		player->reinit();
		delete player;
		shield->reinit();
		delete shield;
		bat->reinit();
		delete bat;
		tree->reinit();
		delete tree;
		fireball->reinit();
		delete fireball;
		potion->reinit();
		delete potion;
	}
	void Game::play(short& _score) {
		init();
		while (!_gameover) {
			update(_score);
			draw(_score);
		}
		reinit();
	}
	void Game::hud(short& _score) {
		DrawRectangle(0, screenHeight - 50, screenWidth, 50, BLACK);
		DrawText("Lifes:", 0, screenHeight - 40, 40, WHITE);
		for (short i = _cantlifes+5;i > 5;i--) {
			DrawRectangle(i * 19+1, poshud + 10, 20, 17, RED);
		}
		DrawTexture(_manabar, 105, poshud, WHITE);
		DrawText("|", 260, poshud - 10, 70, WHITE);
		DrawText("SCORE:", 280, poshud, 40, WHITE);
		DrawText(FormatText("%d", _score), 440, poshud, 40, YELLOW);
		DrawText("|", 570, poshud - 10, 70, WHITE);
		DrawText("TIME:", 600, poshud, 40, WHITE);
		DrawText(FormatText("%d", _minutes), 740, poshud, 40, WHITE);
		DrawText(":", 770, poshud, 40, WHITE);
		DrawText(FormatText("%d", _seconds), 780, poshud, 40, WHITE);
		DrawText("|", 840, poshud - 10, 70, WHITE);
		DrawText("Mana:", 870, poshud,40, WHITE);
		for (short i = _mana+100;i > minmana+100;i--) {
			DrawRectangle(i * 10+2, poshud+9, 20, 19, BLUE);
		}
		DrawTexture(_manabar, 1000, poshud, WHITE);
	}
	void Game::spawnObstacle() {
		if (_spawn == true) {
			if (tree->getActive()) {
				if (_treetype == 1) {
					tree->setX(_treemovement);
					tree->setY(posground - tree->getHeight());
					tree->setHeight(tree->getTree1().height);
					tree->setWidth(tree->getTree1().width);
					DrawTexture(tree->getTree1(), tree->getX(), tree->getY(), WHITE);
					if (_treemovement >= -tree->getWidth()) {
						if (!_pause)
							_treemovement += _proceduralmovement - movementground;
					}
					else {
						if (!_pause) {
							_treemovement = screenWidth;
							_treetype = GetRandomValue(1, 2);
							tree->setActive(true);
						}
					}
				}
				else {
					tree->setX(_treemovement);
					tree->setY(posground - tree->getHeight());
					tree->setHeight(tree->getTree2().height);
					tree->setWidth(tree->getTree2().width);
					DrawTexture(tree->getTree2(), tree->getX(), tree->getY(), WHITE);
					if (_treemovement >= -tree->getWidth()) {
						if (!_pause)
						_treemovement += _proceduralmovement - movementground;
					}
					else {
						if (!_pause) {
							_treemovement = screenWidth;
							_treetype = GetRandomValue(1, 2);
							tree->setActive(true);
						}
					}
				}
			}
			if (bat->getActive()) {
				bat->setX(_batmovement);
				bat->setY(_posbat);
				bat->draw();
				if (_batmovement >= -bat->getWidth()) {
					if (!_pause)
						_batmovement += _proceduralmovement - movementbat;
				}
				else {
					if (!_pause) {
						_posbat = GetRandomValue(1, 2);
						if (_posbat == 1) {
							_posbat = posup;
						}
						else
							_posbat = posmiddle;
						_batmovement = screenWidth;
					}
				}
			}
		}
	}
	void Game::collisionManager(short& _score) {
		if ((CheckCollisionRecs(player->getBoxcollider(), tree->getBoxcollider()) || CheckCollisionRecs(player->getBoxcollider(), bat->getBoxcollider())) && _collisionflag == false) {
			_cantlifes--;
			PlaySound(sound::playerhit);
			shield->init((player->getSizeposx() + player->getSizeposwidth()), (player->getSizeposy() - player->getSizeposheight() / 2));
			_collisionflag = true;
			_timeelapsedshield = durationshield;
			_score -= 50;
		}
		if (CheckCollisionRecs(player->getBoxcollider(), potion->getBoxcollider())&&potion->getActive()) {
			PlaySound(sound::playerhit);
			potion->setActive(false);
			potion->setBoxcolliderX(screenWidth);
			_mana = 10;
		}
		if (CheckCollisionRecs(fireball->getBoxCollider(), tree->getBoxcollider()) && fireball->getActive()) {
			if (tree->getActive())
			PlaySound(sound::hit);
			tree->setActive(false);
			fireball->setActive(false);
			_fireballmovement = player->getSizeposx() + player->getSizeposwidth();
			_shootflag = false;
			_score += 50;
		}
		if (CheckCollisionRecs(fireball->getBoxCollider(), bat->getBoxcollider())&&fireball->getActive()) {
			if (bat->getActive())
			PlaySound(sound::hit);
			bat->setActive(false);
			fireball->setActive(false);
			_fireballmovement = player->getBoxcollider().x + player->getSizeposwidth();
			_shootflag = false;
			_score += 50;
		}

	}
	void Game::spawnPotion() {
		if (potion->getActive()) {
			potion->setBoxcolliderY(posground- potion->getBoxcolliderheight());
			potion->setBoxcolliderX(_potionmovement);
			potion->draw();
			if (_potionmovement >= -potion->getBoxcolliderwidth()) {
				if (!_pause)
					_potionmovement += _proceduralmovement - movementground;
			}
			else {
				potion->setBoxcolliderX(screenWidth);
				potion->setActive(false);
			}
		}
	}
	void Game::pauseMenu() {
		for (short i = 0;i < cantopciones;i++)
			options[i]->draw();
		switch (_input)
		{
		case CHOISE::RESUME:
			options[0]->optionSelection(_input);
			if (IsKeyReleased(KEY_ENTER))
				_pause = false;
			break;
		case CHOISE::GOBACK:
			options[1]->optionSelection(_input);
			if (IsKeyReleased(KEY_ENTER)) {
				_gameover = true;
			}
			break;
		}
	}
}