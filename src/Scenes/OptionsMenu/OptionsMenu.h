#pragma once
#ifndef OPTIONSMENU_H
#define OPTIONSMENU_H
#define MAX_OPTIONS 3
#include "../../Entities/Options/Options.h"
#include "../../Entities/Sound/Sound.h"
#include "../../ScreenSize.h"

namespace optionsmenu {
	class OptionsMenu {
	private:
		options::Options* options[MAX_OPTIONS];
		CHOISE _input;
		Texture2D _background;
		bool backmenu;
		short auxselection;
	public:
		void play();
		void init();
		void update();
		void draw();
		void volumenMenu(short& input);
	};
}
#endif // !OPTIONSMENU_H
