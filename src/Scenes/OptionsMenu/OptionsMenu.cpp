#include "OptionsMenu.h"

namespace optionsmenu {
	void OptionsMenu::init() {
		PlayMusicStream(sound::musicoptions);
		backmenu = false;
		_input = CHOISE::CREDITS;
		_background = LoadTexture("res/Textures/Background/optionsmenu.png");
		Vector2 poscredit;
		Vector2 posvolume;
		Vector2 posback;
		auxselection = 0;
		poscredit.x = screenWidth / 6 - 273 / 2; //_options[0]->getTextBox().width/2;;
		poscredit.y = screenHeight / 4;
		options[0] = new options::Options("CREDITS", poscredit);
		posvolume.x = screenWidth / 6 - options[0]->getTextBox().width / 2;
		posvolume.y = options[0]->getPos().y + options[0]->getTextBox().height;
		options[1] = new options::Options("VOLUME", posvolume);
		posback.x = screenWidth / 6 - options[1]->getTextBox().width / 2;
		posback.y = options[1]->getPos().y + options[1]->getTextBox().height;
		options[2] = new options::Options("GOBACKOPTIONS", posback);

	}
	void OptionsMenu::play() {
		init();
		while(backmenu==false) {
			update();
			draw();
		}
	}
	void OptionsMenu::update() {
		UpdateMusicStream(sound::musicoptions);
		switch (_input)
		{
		case CHOISE::CREDITS:
			options[0]->optionSelection(_input);
			break;
		case CHOISE::VOLUME:
			options[1]->optionSelection(_input);
			break;
		case CHOISE::GOBACKOPTIONS:
			options[2]->optionSelection(_input);
			if (IsKeyReleased(KEY_ENTER)) {
				PlaySound(sound::selectionenter);
				backmenu = true;
			}
			break;
		}
	}
	void OptionsMenu::draw() {
		BeginDrawing();
		ClearBackground(BLACK);
		DrawTextureRec(_background, { 0,0,static_cast<float>(screenWidth),static_cast<float>(screenHeight) }, { 0, 0 }, WHITE);
		for (short i = 0;i < MAX_OPTIONS; i++) {
			options[i]->draw();
		}
		switch (_input)
		{
		case CHOISE::CREDITS:
			DrawText("Made By Gaiusglaber", 0, screenHeight-50, 50, WHITE);
			break;
		case CHOISE::VOLUME:
			DrawText(">", 370, 220, 70, WHITE);
			if (IsKeyPressed(KEY_RIGHT)) {
				short input=5;
				volumenMenu(input);
			}
			break;
		}
		EndDrawing();
	}
	void OptionsMenu::volumenMenu(short& input) {
		while (!IsKeyPressed(KEY_LEFT)) {
			UpdateMusicStream(sound::musicoptions);
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(_background, 0, 0, WHITE);
			for (short i = 0;i < MAX_OPTIONS; i++) {
				options[i]->draw();
			}
			float auxvolumen[6]{ 0,0.05f,0.2f,0.3f,0.4f,0.5f };
			if (input < 5) {
				if (IsKeyPressed(KEY_UP)) {
					PlaySound(sound::selectionarrow);
					input++;
				}
			}
			else input = 5;
			if (input > 0) {

				if (IsKeyPressed(KEY_DOWN)) {
					PlaySound(sound::selectionarrow);
					input--;
				}

			}
			else input = 0;
			for (short i = 0;i < 5;i++) {
				if (input > i) {
					DrawRectangle(450 , 270-(i * 30), 10, 10, WHITE);
				}
			}
			DrawText("+", 435, 90, 70, RED);
			DrawText("-", 435, 275, 70, RED);
			sound::modificarVolumen(auxvolumen, input);
			DrawText("<", 370, 220, 70, RED);

			EndDrawing();
		}
	}	
}
