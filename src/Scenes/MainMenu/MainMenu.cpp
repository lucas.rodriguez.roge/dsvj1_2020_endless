#include "raylib.h"
#include "MainMenu.h"
  

namespace mainmenu {
    
    void MainMenu::play() {
        init();

        while (!WindowShouldClose()&&!_exit)    // Detect window close button or ESC key
        {
            draw();
            update();
        }
        reinit();
        CloseWindow();
    }
    void MainMenu::init() {
        sound::initSounds();
        PlayMusicStream(sound::musicmenu);
        _exit = false;
        _background = LoadTexture("res/Textures/Background/mainmenu.png");
        _title = LoadTexture("res/Textures/Background/title.png");
        _score = 0;
        _maxscore = 0;
        Vector2 pos_play;
        Vector2 pos_options;
        Vector2 pos_exit;

        _input = CHOISE::OPTIONS;

        pos_play.x = screenWidth/2 - 273/2; //_options[0]->getTextBox().width/2;
        pos_play.y = screenHeight / 3;
        _options[0] = new options::Options("PLAY", pos_play);
        pos_options.x = screenWidth / 2 - _options[0]->getTextBox().width/2;
        pos_options.y = _options[0]->getPos().y + _options[0]->getTextBox().height;
        _options[1] = new options::Options("OPTIONS", pos_options);
        pos_exit.x = screenWidth / 2 - _options[0]->getTextBox().width/2;
        pos_exit.y = _options[1]->getPos().y + _options[1]->getTextBox().height;
        _options[2] = new options::Options("EXIT", pos_exit);
    }
    void MainMenu::update() {
        updateScore();
        UpdateMusicStream(sound::musicmenu);
        switch (_input) {
        case CHOISE::PLAY:
            _options[0]->optionSelection(_input);
            if (IsKeyReleased(KEY_ENTER)) {
                PlaySound(sound::selectionenter);
                gameplay::Game* game = new gameplay::Game();
                game->play(_score);
                delete game;
            }
            break;
        case CHOISE::OPTIONS:
            _options[1]->optionSelection(_input);
            if (IsKeyReleased(KEY_ENTER)) {
                PlaySound(sound::selectionenter);
                optionsmenu::OptionsMenu* optionsmenu = new optionsmenu::OptionsMenu();
                optionsmenu->play();
                delete optionsmenu;
            }
            break;
        case CHOISE::EXIT:
            if (IsKeyPressed(KEY_ENTER)) {
                PlaySound(sound::selectionenter);
                _exit = true;
            }
            _options[2]->optionSelection(_input);
            break;
        }


    }
    void MainMenu::draw() {
        BeginDrawing();
        ClearBackground(BLACK);
        DrawTextureRec(_background, {0,0,static_cast<float>(screenWidth),static_cast<float>(screenHeight)}, { 0, 0 }, WHITE);
        DrawTexture(_title, (screenWidth / 2 - _title.width / 2), screenHeight / 8, WHITE);
        DrawText("v1.0.5", 0, screenHeight - 50, 50, WHITE);
        for (short i = 0;i < MAXOPTIONS; i++) {
            _options[i]->draw();
        }
        if (_input == CHOISE::PLAY) {
            DrawText("Highscore", screenWidth / 2 + screenWidth / 4, _options[0]->getPos().y + _options[0]->getTextBox().height - 30, 50, WHITE);
            DrawText(FormatText("%d", _maxscore), screenWidth / 2 + screenWidth / 4+50, _options[0]->getPos().y + _options[0]->getTextBox().height+20, 40, YELLOW);
        }
        EndDrawing();
    }
    void MainMenu::reinit() {
        UnloadTexture(_background);
        CloseAudioDevice();
    }
    void MainMenu::updateScore() {
        if (_score > _maxscore) {
            _maxscore = _score;
            _score = 0;
        }
    }
}
