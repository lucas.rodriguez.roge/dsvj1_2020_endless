#pragma once
#ifndef MAINMENU_H
#define MAINMENU_H
#define MAXOPTIONS 3
#define LEVELS 10


#include "../../ScreenSize.h"
#include "../OptionsMenu/OptionsMenu.h"
#include "../../Entities/Sound/Sound.h"
#include "../Game/Game.h"

enum class SCENES{MAINMENU,OPTIONS,GAME};

namespace mainmenu {
	class MainMenu {
	private:
		options::Options* _options[MAXOPTIONS];
		CHOISE _input;
		Texture2D _background;
		Texture2D _title;
		bool _exit;
		short _score;
		short _maxscore;
	public:
		void init();
		void update();
		void draw();
		void play();
		void reinit();
		void updateScore();
	};
}
#endif // MAINMENU_H