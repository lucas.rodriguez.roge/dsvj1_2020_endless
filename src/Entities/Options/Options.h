#pragma once
#ifndef OPTIONS_H
#define OPTIONS_H
#include <iostream>
#include "raylib.h"
#include "../Sound/Sound.h"

using namespace std;
enum class CHOISE { PLAY, OPTIONS, EXIT, RESUME, GOBACK, CREDITS, CHARACTERSELECTION, GOBACKOPTIONS, VOLUME };

namespace options {
	class Options {
	private:
		string _label;
		Color _color;
		Vector2 _pos;
		Texture2D _textbox;
	public:
		Options(string label, Vector2 pos);
		void optionSelection(CHOISE& _input);
		void draw();
		Vector2 getPos();
		Texture2D getTextBox();
	};
}


#endif // !OPTIONS_H
