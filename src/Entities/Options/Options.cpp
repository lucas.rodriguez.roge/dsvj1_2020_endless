#include "Options.h"
namespace options {

	Options::Options(string label, Vector2 pos) {
		_label = label;
		_color = WHITE;
		_pos = pos;
		_textbox = LoadTexture("res/Textures/Background/textbox2.png");
	}
	void Options::optionSelection(CHOISE& _input) {	
		switch (_input) {
		case CHOISE::PLAY: {
			_color = RED;
			if (IsKeyPressed(KEY_UP))
				PlaySound(sound::selectionerror);
			if (IsKeyPressed(KEY_DOWN)) {
				PlaySound(sound::selectionarrow);
				_color = WHITE;
				_input = CHOISE::OPTIONS;
			}
			break;
		}
		case CHOISE::OPTIONS: {
			_color = RED;
			if (IsKeyPressed(KEY_UP)) {
				PlaySound(sound::selectionarrow);
				_color = WHITE;
				_input = CHOISE::PLAY;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				PlaySound(sound::selectionarrow);
				_color = WHITE;
				_input = CHOISE::EXIT;
			}
			break;
		}
		case CHOISE::EXIT: {
			_color = RED;
			if (IsKeyPressed(KEY_UP)) {
				PlaySound(sound::selectionarrow);
				_color = WHITE;
				_input = CHOISE::OPTIONS;
			}
			if (IsKeyPressed(KEY_DOWN))
				PlaySound(sound::selectionerror);
			break;
		}
		case CHOISE::RESUME: {
			_color = RED;
			if (IsKeyPressed(KEY_UP))
				PlaySound(sound::selectionerror);
			if (IsKeyPressed(KEY_DOWN)) {
				PlaySound(sound::selectionarrow);
				_color = WHITE;
				_input = CHOISE::GOBACK;
			}
			break;
		}
		case CHOISE::GOBACK: {
			_color = RED;
			if (IsKeyPressed(KEY_UP)) {
				PlaySound(sound::selectionarrow);
				_color = WHITE;
				_input = CHOISE::RESUME;
			}
			if (IsKeyPressed(KEY_DOWN))
				PlaySound(sound::selectionerror);
			break;
		}
		case CHOISE::CREDITS:
			_color = RED;
			if (IsKeyPressed(KEY_UP))
				PlaySound(sound::selectionerror);
			if (IsKeyPressed(KEY_DOWN)) {
				PlaySound(sound::selectionarrow);
				_color = WHITE;
				_input = CHOISE::VOLUME;
			}
			break;
		case CHOISE::VOLUME:
			_color = RED;
			if (IsKeyPressed(KEY_DOWN)) {
				PlaySound(sound::selectionarrow);
				_color = WHITE;
				_input = CHOISE::GOBACKOPTIONS;
			}
			if (IsKeyPressed(KEY_UP)) {
				PlaySound(sound::selectionarrow);
				_color = WHITE;
				_input = CHOISE::CREDITS;
			}
			break;
		case CHOISE::GOBACKOPTIONS:
			_color = RED;
			if (IsKeyPressed(KEY_UP)) {
				PlaySound(sound::selectionarrow);
				_color = WHITE;
				_input = CHOISE::VOLUME;
			}
			if (IsKeyPressed(KEY_DOWN))
				PlaySound(sound::selectionerror);
			break;
		}
		}
	void Options::draw() {

		if (_label == "PLAY") {
			DrawTexture(_textbox, _pos.x - 60, _pos.y - 20, WHITE);
			DrawText("PLAY", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "OPTIONS") {
			DrawTexture(_textbox, _pos.x - 60, _pos.y - 20, WHITE);
			DrawText("OPTIONS", _pos.x-51, _pos.y, 57, _color);
		}
		else if (_label == "EXIT") {
			DrawTexture(_textbox, _pos.x - 60, _pos.y - 20, WHITE);
			DrawText("EXIT", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "RESUME") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("RESUME", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "GOBACK") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("BACK", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "CREDITS") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("CREDITS", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "PADDLE") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("PADDLE", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "VOLUME") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("VOLUME", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "GOBACKOPTIONS") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("BACK", _pos.x + 50, _pos.y, 60, _color);
		}


	}
	Vector2 Options::getPos() {
		return _pos;
	}
	Texture2D Options::getTextBox() {
		return _textbox;
	}

}