#include "Obstacles.h"
namespace obstacle {
	void Tree::init() {
		_tree[0] = LoadTexture("res/Textures/Objects/tree1.png");
		_tree[1] = LoadTexture("res/Textures/Objects/tree2.png");
		_boxcollider.width = _tree[1].width;
		_boxcollider.height = _tree[1].height;
		_active = true;
	}
	void Tree::reinit() {
		UnloadTexture(_tree[0]);
		UnloadTexture(_tree[1]);
	}
	void Tree::setX(float x) {
		_boxcollider.x = x;
	}
	void Tree::setY(float y) {
		_boxcollider.y = y;
	}
	void Tree::setWidth(float width) {
		_boxcollider.width = width;
	}
	void Tree::setHeight(float height) {
		_boxcollider.height = height;
	}
	void Tree::setActive(bool active) {
		_active = active;
	}
	Rectangle Tree::getBoxcollider() {
		return _boxcollider;
	}
	float Tree::getX() {
		return _boxcollider.x;
	}
	float Tree::getY() {
		return _boxcollider.y;
	}
	float Tree::getWidth() {
		return _boxcollider.width;
	}
	float Tree::getHeight() {
		return _boxcollider.height;
	}
	bool Tree::getActive() {
		return _active;
	}
	Texture2D Tree::getTree1() {
		return _tree[0];
	}
	Texture2D Tree::getTree2() {
		return _tree[1];
	}
	void Bat::init() {
		_bat[0] = LoadTexture("res/Textures/Objects/BatMovement/bat1.png");
		_bat[1] = LoadTexture("res/Textures/Objects/BatMovement/bat2.png");
		_bat[2] = LoadTexture("res/Textures/Objects/BatMovement/bat3.png");
		_bat[3] = LoadTexture("res/Textures/Objects/BatMovement/bat4.png");
		_bat[4] = LoadTexture("res/Textures/Objects/BatMovement/bat5.png");
		_bat[5] = LoadTexture("res/Textures/Objects/BatMovement/bat6.png");
		_bat[6] = LoadTexture("res/Textures/Objects/BatMovement/bat7.png");
		_bat[7] = LoadTexture("res/Textures/Objects/BatMovement/bat8.png");
		_batframe = 0;
		_active = true;
	}
	void Bat::draw() {
		if (_batframe >= 0 && _batframe <= 3) {
			DrawTexture(_bat[0], _boxcollider.x, _boxcollider.y, WHITE);
			setHeight(_bat[0].height);
			setWidth(_bat[0].width);
			_batframe++;
		}
		if (_batframe >= 3 && _batframe <= 6) {
			DrawTexture(_bat[1], _boxcollider.x, _boxcollider.y, WHITE);
			setHeight(_bat[1].height);
			setWidth(_bat[1].width);
			_batframe++;
		}
		if (_batframe >= 6 && _batframe <= 9) {
			DrawTexture(_bat[2], _boxcollider.x, _boxcollider.y, WHITE);
			setHeight(_bat[2].height);
			setWidth(_bat[2].width);
			_batframe++;
		}
		if (_batframe >= 9 && _batframe <= 12) {
			DrawTexture(_bat[3], _boxcollider.x, _boxcollider.y, WHITE);
			setHeight(_bat[3].height);
			setWidth(_bat[3].width);
			_batframe++;
		}
		if (_batframe >= 12 && _batframe <= 15) {
			DrawTexture(_bat[4], _boxcollider.x, _boxcollider.y, WHITE);
			setHeight(_bat[4].height);
			setWidth(_bat[4].width);
			_batframe++;
		}
		if (_batframe >= 15 && _batframe <= 18) {
			DrawTexture(_bat[5], _boxcollider.x, _boxcollider.y, WHITE);
			setHeight(_bat[5].height);
			setWidth(_bat[5].width);
			_batframe++;
		}
		if (_batframe >= 18 && _batframe <= 21) {
			DrawTexture(_bat[6], _boxcollider.x, _boxcollider.y, WHITE);
			setHeight(_bat[6].height);
			setWidth(_bat[6].width);
			_batframe++;
		}
		if (_batframe >= 21 && _batframe <= 24) {
			DrawTexture(_bat[7], _boxcollider.x, _boxcollider.y, WHITE);
			setHeight(_bat[7].height);
			setWidth(_bat[7].width);
			_batframe++;
		}
		if (_batframe >= 24)
			_batframe = 0;
	}
	void Bat::reinit() {
		for (short i = 0;i < cantbatsprites;i++) {
			UnloadTexture(_bat[i]);
		}
	}
	void Bat::setX(float x) {
		_boxcollider.x = x;
	}
	void Bat::setY(float y) {
		_boxcollider.y = y;
	}
	void Bat::setWidth(float width) {
		_boxcollider.width = width;
	}
	void Bat::setHeight(float height) {
		_boxcollider.height = height;
	}
	void Bat::setActive(bool active) {
		_active = active;
	}
	Rectangle Bat::getBoxcollider() {
		return _boxcollider;
	}
	float Bat::getX() {
		return _boxcollider.x;
	}
	float Bat::getY() {
		return _boxcollider.y;
	}
	float Bat::getWidth() {
		return _boxcollider.width;
	}
	float Bat::getHeight() {
		return _boxcollider.height;
	}
	bool Bat::getActive() {
		return _active;
	}
}