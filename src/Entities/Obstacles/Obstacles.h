#pragma once
#ifndef OBSTACLES_H
#define OBSTACLES_H
#include "raylib.h" 
#include "../../ScreenSize.h"
const short cantbatsprites = 8;
const short canttreesprites = 2;
namespace obstacle {
	class Tree {
	private:
		Texture2D _tree[canttreesprites];
		Rectangle _boxcollider;
		bool _active;
	public:
		void init();
		void reinit();
		void setX(float x);
		void setY(float y);
		void setWidth(float width);
		void setHeight(float height);
		void setActive(bool active);
		Rectangle getBoxcollider();
		float getX();
		float getY();
		float getWidth();
		float getHeight();
		Texture2D getTree1();
		Texture2D getTree2();
		bool getActive();
	};
	class Bat {
	private:
		Texture2D _bat[cantbatsprites];
		Rectangle _boxcollider;
		short _batframe;
		bool _active;
	public:
		void init();
		void draw();
		void reinit();
		void setX(float x);
		void setY(float y);
		void setWidth(float width);
		void setHeight(float height);
		void setActive(bool active);
		Rectangle getBoxcollider();
		float getX();
		float getY();
		float getWidth();
		float getHeight();
		bool getActive();
	};
}
#endif // !OBSTACLES_H