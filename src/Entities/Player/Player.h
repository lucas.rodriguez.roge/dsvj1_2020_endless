#pragma once

#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "../../ScreenSize.h"
#include "../Sound/Sound.h"

const short framesmovement=4;
enum class RAIL { DOWN=300, MIDDLE=150, UP=0 };
class Player {
private:
	Texture2D _movement[framesmovement];
	Rectangle _boxcollider;
	short _playerframe;
	float _jumpspeed;
public:
	void init();
	void update();
	void draw();
	void reinit();
	void controlMovement();
	Rectangle getBoxcollider();
	float getSizeposx();
	float getSizeposy();
	float getSizeposwidth();
	float getSizeposheight();
	void setSizeposx(float x);
	void setSizeposy(float y);
	void setSizeposwidth(float width);
	void setSizeposheight(float height);
};
#endif // !PLAYER_H
