#include "Player.h"

void Player::init() {
	_movement[0] = LoadTexture("res/Textures/Movement/Movement1.png");
	_movement[1] = LoadTexture("res/Textures/Movement/Movement2.png");
	_movement[2] = LoadTexture("res/Textures/Movement/Movement3.png");
	_movement[3] = LoadTexture("res/Textures/Movement/Movement4.png");
	_boxcollider.x = 200;
	_boxcollider.y = static_cast<float>(RAIL::DOWN);
	_playerframe = 0;
	_jumpspeed = 5.5f;
}
void Player::update() {
	controlMovement();
}
void Player::draw() {
	if (_playerframe >= 0 && _playerframe <= 10) {
		DrawTexture(_movement[0], _boxcollider.x, _boxcollider.y, WHITE);
		setSizeposheight(_movement[0].height);
		setSizeposwidth(_movement[0].width);
		_playerframe++;
	}
	if (_playerframe >= 10 && _playerframe <= 20) {
		DrawTexture(_movement[1], _boxcollider.x, _boxcollider.y, WHITE);
		setSizeposheight(_movement[1].height);
		setSizeposwidth(_movement[1].width);
		_playerframe++;
	}
	if (_playerframe >= 20 && _playerframe <= 30) {
		DrawTexture(_movement[2], _boxcollider.x, _boxcollider.y, WHITE);
		setSizeposheight(_movement[2].height);
		setSizeposwidth(_movement[2].width);
		_playerframe++;
	}
	if (_playerframe >= 30 && _playerframe <= 40) {
		DrawTexture(_movement[3], _boxcollider.x, _boxcollider.y, WHITE);
		setSizeposheight(_movement[3].height);
		setSizeposwidth(_movement[3].width);
		_playerframe++;
	}
	if (_playerframe >= 40)
		_playerframe = 0;

}
void Player::reinit(){
	for (short i = 0;i < framesmovement;i++) {
		UnloadTexture(_movement[i]);
	}
}
void Player::controlMovement() {
	if (IsKeyPressed(KEY_UP)) {
		switch (static_cast<RAIL>(_boxcollider.y)) {
		case RAIL::UP:
			PlaySound(sound::selectionerror);
			break;
		case RAIL::MIDDLE:
			PlaySound(sound::playerbeep);
			_boxcollider.y=static_cast<float>(RAIL::UP);
			break;
		case RAIL::DOWN:
			PlaySound(sound::playerbeep);
			_boxcollider.y = static_cast<float>(RAIL::MIDDLE);
			break;
		}
	}
	if (IsKeyPressed(KEY_DOWN)) {
		
		switch (static_cast<RAIL>(_boxcollider.y)) {
		case RAIL::UP:
			PlaySound(sound::playerbeep);
			_boxcollider.y = static_cast<float>(RAIL::MIDDLE);
			break;
		case RAIL::MIDDLE:
			PlaySound(sound::playerbeep);
			_boxcollider.y = static_cast<float>(RAIL::DOWN);
			break;
		case RAIL::DOWN:
			PlaySound(sound::selectionerror);
			break;
		}
	}
}
Rectangle Player::getBoxcollider() {
	return _boxcollider;
}
float Player::getSizeposx() {
	return _boxcollider.x;
}
float Player::getSizeposy() {
	return _boxcollider.y;
}
float Player::getSizeposwidth() {
	return _boxcollider.width;
}
float Player::getSizeposheight() {
	return _boxcollider.height;
}
void Player::setSizeposx(float x) {
	_boxcollider.x = x;
}
void Player::setSizeposy(float y) {
	_boxcollider.y = y;
}
void Player::setSizeposwidth(float width) {
	_boxcollider.width = width;
}
void Player::setSizeposheight(float height) {
	_boxcollider.height = height; 
}