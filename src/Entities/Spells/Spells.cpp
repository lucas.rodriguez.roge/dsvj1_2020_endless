#include "Spells.h"

namespace spells {
	void Fireball::init() {
		_movement[0] = LoadTexture("res/Textures/Objects/Fireball/fireball1.png");
		_movement[1] = LoadTexture("res/Textures/Objects/Fireball/fireball2.png");
		_movement[2] = LoadTexture("res/Textures/Objects/Fireball/fireball3.png");
		_movement[3] = LoadTexture("res/Textures/Objects/Fireball/fireball4.png");
		_fireballframe = 0;
		_boxcollider.x = -200;
		_boxcollider.y = -200;
		_boxcollider.width = _movement[0].width;
		_boxcollider.height = _movement[0].height;
		_active = true;
	}
	void Fireball::draw() {
		if (_fireballframe >= 0 && _fireballframe <= 10) {
			DrawTexture(_movement[0], _boxcollider.x, _boxcollider.y, WHITE);
			setBoxColliderHeight(_movement[0].height);
			setBoxColliderHeight(_movement[0].width);
		}
		if (_fireballframe >= 10 && _fireballframe <= 20) {
			DrawTexture(_movement[1], _boxcollider.x, _boxcollider.y, WHITE);
			setBoxColliderHeight(_movement[1].height);
			setBoxColliderHeight(_movement[1].width);
		}
		if (_fireballframe >= 20 && _fireballframe <= 30) {
			DrawTexture(_movement[2], _boxcollider.x, _boxcollider.y, WHITE);
			setBoxColliderHeight(_movement[2].height);
			setBoxColliderHeight(_movement[2].width);
		}
		if (_fireballframe >= 30 && _fireballframe <= 40) {
			DrawTexture(_movement[3], _boxcollider.x, _boxcollider.y, WHITE);
			setBoxColliderHeight(_movement[3].height);
			setBoxColliderHeight(_movement[3].width);
		}
		if (_fireballframe >= 40) {
			_fireballframe = 0;
		}
		_fireballframe++;
	}
	void Fireball::reinit() {
		for (short i=0;i<cantfireballsprites;i++)
		UnloadTexture(_movement[i]);
	}
	void Fireball::setBoxColliderX(float x) {
		_boxcollider.x = x;
	}
	void Fireball::setBoxColliderY(float y) {
		_boxcollider.y = y;
	}
	void Fireball::setBoxColliderWidth(float width) {
		_boxcollider.width = width;
	}
	void Fireball::setBoxColliderHeight(float height) {
		_boxcollider.height = height;
	}
	void Fireball::setBoxCollider(Rectangle boxcollider) {
		_boxcollider = boxcollider;
	}
	void Fireball::setActive(bool active) {
		_active = active;
	}
	float Fireball::getBoxColliderX() {
		return _boxcollider.x;
	}
	float Fireball::getBoxColliderY() {
		return _boxcollider.y;
	}
	float Fireball::getBoxColliderWidth() {
		return _boxcollider.width;
	}
	float Fireball::getBoxColliderHeight() {
		return _boxcollider.height;
	}
	Rectangle Fireball::getBoxCollider() {
		return _boxcollider;
	}
	bool Fireball::getActive() {
		return _active;
	}
}