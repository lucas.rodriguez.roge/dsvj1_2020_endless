#pragma once
#ifndef SPELLS_H
#define SPELLS_H

#include "raylib.h"


namespace spells {
	const short cantfireballsprites = 4;
	class Fireball {
	private:
		Texture2D _movement[cantfireballsprites];
		Rectangle _boxcollider;
		short _fireballframe;
		bool _active;
	public:
		void init();
		void draw();
		void reinit();
		void setBoxColliderX(float x);
		void setBoxColliderY(float y);
		void setBoxColliderWidth(float width);
		void setBoxColliderHeight(float height);
		void setBoxCollider(Rectangle boxcollider);
		void setActive(bool active);
		float getBoxColliderX();
		float getBoxColliderY();
		float getBoxColliderWidth();
		float getBoxColliderHeight();
		Rectangle getBoxCollider();
		bool getActive();
	};
}

#endif // !SPELLS_H
