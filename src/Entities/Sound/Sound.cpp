#include "Sound.h"
namespace sound {
	Sound playerbeep = LoadSound("res/Sound/Select 3.wav");
	Sound playerhit = LoadSound("res/Sound/playerhit.mp3");
	Sound shoot = LoadSound("res/Sound/Shoot 3.wav");
	Sound hit = LoadSound("res/Sound/Hit 5.wav");
	Sound selectionarrow = LoadSound("res/Sound/Select 2.wav");
	Sound selectionerror = LoadSound("res/Sound/Wrong 3.wav");
	Sound selectionenter = LoadSound("res/Sound/Select 4.wav");
	Music musicmenu = LoadMusicStream("res/Sound/mainmenu.mp3");
	Music musicgame = LoadMusicStream("res/Sound/gameplaymusic.mp3");
	Music musicoptions = LoadMusicStream("res/Sound/optionsmenu.mp3");
	void initSounds() {
		InitAudioDevice();
		SetSoundVolume(playerbeep, 0.5f);
		SetSoundVolume(playerhit, 0.5f);
		SetSoundVolume(shoot, 0.5f);
		SetSoundVolume(hit, 0.5f);
		SetSoundVolume(selectionarrow, 0.5f);
		SetSoundVolume(selectionerror, 0.5f);
		SetSoundVolume(selectionenter, 0.5f);
		SetMusicVolume(musicmenu, 0.5f);
		SetMusicVolume(musicgame, 0.5f);
		SetMusicVolume(musicoptions, 0.5f);
	}
	void modificarVolumen(float auxvolumen[], short input) {
		SetSoundVolume(playerbeep, auxvolumen[input]);
		SetSoundVolume(playerhit, auxvolumen[input]);
		SetSoundVolume(shoot, auxvolumen[input]);
		SetSoundVolume(hit, auxvolumen[input]);
		SetSoundVolume(selectionarrow, auxvolumen[input]);
		SetSoundVolume(selectionerror, auxvolumen[input]);
		SetSoundVolume(selectionenter, auxvolumen[input]);
		SetMusicVolume(musicmenu, auxvolumen[input]);
		SetMusicVolume(musicgame, auxvolumen[input]);
		SetMusicVolume(musicoptions, auxvolumen[input]);
	}
}