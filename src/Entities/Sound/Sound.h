#pragma once
#ifndef SOUND_H
#define SOUND_H
#include "raylib.h"

#endif // !SOUND_H
namespace sound {
	extern Sound playerbeep;
	extern Sound playerhit;
	extern Sound shoot;
	extern Sound hit;
	extern Sound selectionarrow;
	extern Sound selectionerror;
	extern Sound selectionenter;
	extern Music musicmenu;
	extern Music musicgame;
	extern Music musicoptions;

	extern void initSounds();
	extern void modificarVolumen(float auxvolumen[], short input);
}