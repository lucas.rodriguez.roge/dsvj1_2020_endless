#include "Powerups.h"

namespace powerups {
	void Shield::init(float x,float y) {
		_texture = LoadTexture("res/Textures/Powerups/shield.png");
		_boxcollider.x = x;
		_boxcollider.y = y;
		_boxcollider.width = _texture.width;
		_boxcollider.height = _texture.height;
	}
	void Shield::draw() {
		DrawTexture(_texture, _boxcollider.x, _boxcollider.y, WHITE);
	}
	void Shield::reinit() {
		UnloadTexture(_texture);
	}
	void Shield::setBoxcolliderX(float x) {
		_boxcollider.x = x;
	}
	void Shield::setBoxcolliderY(float y) {
		_boxcollider.y = y;
	}
	void Shield::setBoxcolliderwidth(float width) {
		_boxcollider.width = width;
	}
	void Shield::setBoxcolliderheight(float height) {
		_boxcollider.height = height;
	}
	float Shield::getBoxcolliderX() {
		return _boxcollider.x;
	}
	float Shield::getBoxcolliderY() {
		return _boxcollider.y;
	}
	float Shield::getBoxcolliderwidth() {
		return _boxcollider.width;
	}
	float Shield::getBoxcolliderheight() {
		return _boxcollider.height;
	}

	//---------------------------------------------------------------

	void Potion::init() {
		_texture[0] = LoadTexture("res/Textures/Objects/Potion/pot1.png");
		_texture[1] = LoadTexture("res/Textures/Objects/Potion/pot2.png");
		_texture[2] = LoadTexture("res/Textures/Objects/Potion/pot3.png");
		_texture[3] = LoadTexture("res/Textures/Objects/Potion/pot4.png");
		_texture[4] = LoadTexture("res/Textures/Objects/Potion/pot5.png");
		_texture[5] = LoadTexture("res/Textures/Objects/Potion/pot6.png");
		_texture[6] = LoadTexture("res/Textures/Objects/Potion/pot7.png");
		_texture[7] = LoadTexture("res/Textures/Objects/Potion/pot8.png");
		_texture[8] = LoadTexture("res/Textures/Objects/Potion/pot9.png");
		_texturemovement = 0;
		_active = false;
	}
	void Potion::draw() {
		if (_texturemovement >= 0 && _texturemovement <= 4) {
			DrawTexture(_texture[0], _boxcollider.x, _boxcollider.y, WHITE);
			setBoxcolliderheight(_texture[8].height);
			setBoxcolliderwidth(_texture[8].width);
		}
		if (_texturemovement >= 4 && _texturemovement <= 8) {
			DrawTexture(_texture[1], _boxcollider.x, _boxcollider.y, WHITE);
		}
		if (_texturemovement >= 8 && _texturemovement <= 12) {
			DrawTexture(_texture[2], _boxcollider.x, _boxcollider.y, WHITE);
		}
		if (_texturemovement >= 12 && _texturemovement <= 16) {
			DrawTexture(_texture[3], _boxcollider.x, _boxcollider.y, WHITE);
		}
		if (_texturemovement >= 16 && _texturemovement <= 20) {
			DrawTexture(_texture[4], _boxcollider.x, _boxcollider.y, WHITE);
		}
		if (_texturemovement >= 20 && _texturemovement <= 24) {
			DrawTexture(_texture[5], _boxcollider.x, _boxcollider.y, WHITE);
		}
		if (_texturemovement >= 24 && _texturemovement <= 28) {
			DrawTexture(_texture[6], _boxcollider.x, _boxcollider.y, WHITE);
		}
		if (_texturemovement >= 28 && _texturemovement <= 32) {
			DrawTexture(_texture[7], _boxcollider.x, _boxcollider.y, WHITE);
		}
		if (_texturemovement >= 32 && _texturemovement <= 36) {
			DrawTexture(_texture[8], _boxcollider.x, _boxcollider.y, WHITE);
		}
		if (_texturemovement >= 36)
			_texturemovement = 0;
		_texturemovement++;
	}
	void Potion::reinit() {
		for (short i = 0;i < cantmanapotionsprite;i++) {
			UnloadTexture(_texture[i]);
		}
	}
	void Potion::setBoxcolliderX(float x) {
		_boxcollider.x = x;
	}
	void Potion::setBoxcolliderY(float y) {
		_boxcollider.y = y;
	}
	void Potion::setBoxcolliderwidth(float width) {
		_boxcollider.width = width;
	}
	void Potion::setBoxcolliderheight(float height) {
		_boxcollider.height = height;
	}
	void Potion::setActive(bool active) {
		_active = active;
	}
	float Potion::getBoxcolliderX() {
		return _boxcollider.x;
	}
	float Potion::getBoxcolliderY() {
		return _boxcollider.y;
	}
	float Potion::getBoxcolliderwidth() {
		return _boxcollider.width;
	}
	float Potion::getBoxcolliderheight() {
		return _boxcollider.height;
	}
	bool Potion::getActive() {
		return _active;
	}
	Rectangle Potion::getBoxcollider() {
		return _boxcollider;
	}
}