#pragma once
#ifndef POWERUPS_H
#define POWERUPS_H

#include "raylib.h"

namespace powerups {
	const short cantmanapotionsprite = 9;
	class Shield {
	private:
		Texture2D _texture;
		Rectangle _boxcollider;
	public:
		void init(float x, float y);
		void draw();
		void reinit();
		void setBoxcolliderX(float x);
		void setBoxcolliderY(float y);
		void setBoxcolliderwidth(float width);
		void setBoxcolliderheight(float height);
		float getBoxcolliderX();
		float getBoxcolliderY();
		float getBoxcolliderwidth();
		float getBoxcolliderheight();
	};
	class Potion {
	private:
		Texture2D _texture[cantmanapotionsprite];
		Rectangle _boxcollider;
		short _texturemovement;
		bool _active;
	public:
		void init();
		void draw();
		void reinit();
		void setBoxcolliderX(float x);
		void setBoxcolliderY(float y);
		void setBoxcolliderwidth(float width);
		void setBoxcolliderheight(float height);
		void setActive(bool active);
		float getBoxcolliderX();
		float getBoxcolliderY();
		float getBoxcolliderwidth();
		float getBoxcolliderheight();
		bool getActive();
		Rectangle getBoxcollider();
	};
}
#endif // !POWERUPS_H
